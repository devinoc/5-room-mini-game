print ("\n")
print ("\n")
print ("\n")
print ("\n")
print ("\n")
print ("\n")
print ("\n")
print ("\n")
print ("\n")
print ("You are in a building with 5 rooms in it.")
print ("Whatever room you choose, you must endure what lies inside.")
print ("Your goal is to survive.")
print ("It is dark because the power is out.")
print ("\n")
print ("\n")
print ("\n")
print ("Room number 1 has a huge pool full of man eating sharks.")
print ("Room number 2 has poisonous snakes.")
print ("Room number 3 has blood sucking bats.")
print ("Room number 4 has the deadly electrical chair.")
print ("Room number 5 has a lethal mass murderer.")
print ("\n")
print ("Which room do you go through?")
print ("1?")
print ("2?")
print ("3?")
print ("4?")
print ("5?")
print ("Type your answer followed by the enter key.")

print ("\n")
print ("\n")
print ("\n")

room = input("> ")

if room == "1" or "one":
    print ("\n")
    print ("You land in the pool and the sharks feed off of you.\nBetter luck next time!\n \nHint: Reread the info. :)")
    print ("\n")
    print ("\n")

if room == "2" or "two":
    print ("\n")
    print ("You get bit multiple times by the snakes.\nYou feel dizzy and fall into an eternal sleep.\nBetter luck next time!\n \nHint: Reread the info. :)")
    print ("\n")
    print ("\n")

if room == "3" or "three":
    print ("\n")
    print ("The bats are all over you.\nYou cant fight them off.\nYou feel dizzy and pass out.\nYou never wake up.\nBetter luck next time!\n \nHint: Reread the info. :)")
    print ("\n")
    print ("\n")

if room == "4" or "four":
    print ("\n")
    print ("You fear the worst. \nYou slowly walk to the chair and sit down.....\nNothing happens.\nThe power was out.\nCongradulations!!!.\nYou have survived! :)")
    print ("\n")
    print ("\n")

if room == "5" or "five":
    print ("\n")
    print ("The murderer gives you a chilling stare.\nHe pulls out a knife and before you even know what happened...\nHe impales you with the knife. \nYou bleed to death.\nBetter luck next time!\n \nHint: Reread the info. :)")
    print ("\n")
    print ("\n")

if room == "Dev room" or "dev room":
    print ("Hey... How did you know this room existed?\nWell you win I guess.\nCongradulations! :)")
    print ("\n")
    print ("\n")