room_options = {
    1: {
        "room": "a huge pool full of man eating sharks",
        "result": "You land in the pool and the sharks feed off of you.",
        "survive": False
    },
    2: {
        "room": "poisonous snakes",
        "result": "You get bit multiple times by the snakes.\nYou feel dizzy and fall into an eternal sleep.",
        "survive": False,
    },
    3: {
        "room": "blood sucking bats",
        "result": "The bats are all over you.\nYou cant fight them off.\nYou feel dizzy and pass out.\nYou never wake up.",
        "survive": False
    },
    4: {
        "room": "a deadly electric chair",
        "result": "You fear the worst. \nYou slowly walk to the chair and sit down.....\nNothing happens.\nThe power was out.\nCongratulations!!!",
        "survive": True
    }, 
    5: {
        "room": "a lethal, mass-murderer",
        "result": "The murderer gives you a chilling stare.\nHe pulls out a knife and before you even know what happened...\nHe impales you with the knife. \nYou bleed to death.",
        "survive": False
    }
}
# Get the room #'s from the dictionary and make them an option to check against
choices = sorted(room_options.keys())

def print_new_line(count=0):
    """ Uses list comprehension to reduce the new lines from repeating in code"""
    print("".join(["\n",] * count))

if __name__ == "__main__":
    print_new_line(3)
    print("""You find yourself in an abandoned,  dark building with no power.  
Through the darkness, you peer 5 doors, unsure of which one to pick.
Whatever room you choose, you must endure what lies inside.\n
Your goal is to survive...""")
    print_new_line(1)
    user_choices = []
    # Loop through dictionary and append the output to a list for later use
    for key, obj in room_options.iteritems():
        user_choices.append("Room #{room} has {description}".format(room=key, description=obj["room"]))

    # Print each item in the list
    print("\n".join(user_choices))

    selected_choice = 0
    while selected_choice not in choices:
        # Stay in the loop if they don't enter the right number
        selected_choice = raw_input("\nWhich room do you choose? [{options}] ".format(options=",".join([str(choice) for choice in choices])))
        try:
            selected_choice = int(selected_choice)
        except:
            selected_choice = 0

    print_new_line(2)

    if room_options[selected_choice]["survive"] == True:
        # Winner winner, chicken dinner.
        print(room_options[selected_choice]["result"])
    else:
        # oh noes
        result = room_options[selected_choice]["result"] + "\nBetter luck next time!\n \nHint: Reread the info. :)"
        print(result)

    print_new_line(1)
